import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { Modify } from 'src/utils/modifyType';
import { Booking } from '../model/booking.model';

@Injectable()
export class BookingService {
  bookingCollection: Collection<
    Modify<
      Booking,
      {
        _id?: ObjectId;
      }
    >
  >;
  constructor(@Inject('DATABASE_CONNECTION') private database: Db) {
    this.bookingCollection = this.database.collection('booking');
  }
}
