import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { paginate } from 'src/utils/paginate';
import { Booking, BookingGraph } from '../model/booking.model';
import { CreateBooking } from '../model/createBooking.args';
import { BookingService } from '../service/booking.service';

@Resolver()
export class BookingResolver {
  constructor(private bookingService: BookingService) {}

  @Mutation(() => BookingGraph)
  async createBooking(@Args() args: CreateBooking) {
    const booking: Booking = {
      ...args,
      _id: new ObjectId(),
      workTimesUserIds: args.workTimesUserIds.map((e) => new ObjectId(e)),
      workTimesObj: args.workTimesObj,
      clubId: new ObjectId(args.clubId),
    };
    await this.bookingService.bookingCollection.insertOne(booking);
    const bookingResponce = new BookingGraph({ ...booking });
    return bookingResponce;
  }

  @Query(() => [BookingGraph])
  async getBookingsOfClub(
    @Args('clubId', { type: () => String }) clubId: string,
    @Args('page', { type: () => Int, defaultValue: 0 }) page: number,
  ): Promise<BookingGraph[]> {
    const bookingsCursor = this.bookingService.bookingCollection.find({
      clubId: new ObjectId(clubId),
    });
    const bookings = await paginate({
      cursor: bookingsCursor,
      page,
      elementsPerPage: 10,
    });
    const bookingsResponce = bookings.map(
      (val) => new BookingGraph({ ...val }),
    );
    return bookingsResponce;
  }
}
