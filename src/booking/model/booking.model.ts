import {
  Field,
  Float,
  GraphQLISODateTime,
  Int,
  ObjectType,
} from '@nestjs/graphql';
import { ObjectId } from 'mongodb';

import { Modify } from 'src/utils/modifyType';
import { WorkTimesObj, WorkTimesObjGraph } from './work_time_obj';

export interface Booking {
  _id: ObjectId;
  name: string;
  description?: string;
  // Id мойки
  clubId: ObjectId;
  price: number;
  timePerMinute: number;
  workTimesUserIds: ObjectId[];
  workTimesObj: WorkTimesObj[][];
  expireDate: Date;
  count: number;
  percent: number;
}

@ObjectType()
export class BookingGraph
  implements
    Modify<
      Booking,
      {
        _id: string;
        clubId: string;
        workTimesUserIds: string[];
      }
    >
{
  @Field()
  _id: string;

  @Field()
  name: string;

  @Field({ nullable: true })
  description?: string;

  @Field(() => Float)
  price: number;

  @Field()
  timePerMinute: number;

  @Field()
  clubId: string;

  // @Field(() => ClubGraph, { nullable: true })
  // club?: ClubGraph;

  // @Field(() => [TrainerBookingGraph], { nullable: true })
  // trainerBooking?: TrainerBookingGraph[];

  // @Field(() => [UserGraph], { nullable: true })
  // trainers?: UserGraph[];

  @Field(() => GraphQLISODateTime)
  expireDate: Date;

  @Field(() => Int)
  count: number;

  @Field()
  percent: number;

  @Field(() => [String], {
    defaultValue: [],
  })
  workTimesUserIds: string[];

  @Field(() => [[WorkTimesObjGraph]])
  workTimesObj: WorkTimesObjGraph[][];

  constructor(
    booking: Partial<Booking> & {
      // club?: Club;
      // trainers?: User[];
    },
  ) {
    if (booking._id != null) this._id = booking._id.toHexString();
    // if (booking.club != null) this.club = new ClubGraph({ ...booking.club });
    if (booking.clubId != null) this.clubId = booking.clubId.toHexString();
    if (booking.name != null) this.name = booking.name;
    if (booking.price != null) this.price = booking.price;
    if (booking.timePerMinute != null)
      this.timePerMinute = booking.timePerMinute;
    if (booking.description != null) this.description = booking.description;
    if (booking.workTimesUserIds != null)
      this.workTimesUserIds = booking.workTimesUserIds.map((e) =>
        e.toHexString(),
      );

    if (booking.workTimesObj != null) this.workTimesObj = booking.workTimesObj;

    if (booking.expireDate != null) this.expireDate = booking.expireDate;
    if (booking.count != null) this.count = booking.count;
    if (booking.percent != null) this.percent = booking.percent;

    // if (booking.trainers != null)
    //   this.trainers = booking.trainers.map((e) => new UserGraph(e));
  }
}
