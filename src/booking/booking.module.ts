import { Module } from '@nestjs/common';
import { BookingResolver } from './resolver/booking.resolver';
import { RecordResolver } from './resolver/record.resolver';
import { BookingService } from './service/booking.service';

@Module({
  imports: [],
  providers: [BookingResolver, BookingService, RecordResolver],
  exports: [BookingService],
})
export class BookingModule {}
