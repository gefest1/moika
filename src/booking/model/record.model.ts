import { ObjectId } from 'mongodb';
import { Modify } from 'src/utils/modifyType';
import { ObjectType, Field, GraphQLISODateTime } from '@nestjs/graphql';

export interface Record {
  _id: ObjectId;
  date: Date;
  endDate: Date;
  bookingId: ObjectId;
  price: number;
  workerId: ObjectId;
  userId: ObjectId;
  clubId: ObjectId;
}

@ObjectType()
export class RecordGraph
  implements
    Modify<
      Record,
      {
        _id: string;
        bookingId: string;
        workerId: string;
        userId: string;
        clubId: string;
      }
    >
{
  @Field()
  _id: string;

  @Field(() => GraphQLISODateTime)
  date: Date;

  @Field(() => GraphQLISODateTime)
  endDate: Date;

  @Field()
  bookingId: string;

  @Field()
  price: number;

  @Field()
  workerId: string;

  @Field()
  userId: string;

  @Field()
  clubId: string;

  constructor(
    record: Partial<Record> & {
      //
    },
  ) {
    if (record._id != null) this._id = record._id.toHexString();
    if (record.date != null) this.date = record.date;
    if (record.endDate != null) this.endDate = record.endDate;
    if (record.bookingId != null)
      this.bookingId = record.bookingId.toHexString();
    if (record.price != null) this.price = record.price;
    if (record.workerId != null) this.workerId = record.workerId.toHexString();
    if (record.clubId != null) this.clubId = record.clubId.toHexString();
  }
}
