import { Inject } from '@nestjs/common';
import { Args, GraphQLISODateTime, Mutation, Resolver } from '@nestjs/graphql';
import { Collection, Db, ObjectId } from 'mongodb';
import { Modify } from 'src/utils/modifyType';
import { Booking } from '../model/booking.model';
import { Record, RecordGraph } from '../model/record.model';

@Resolver()
export class RecordResolver {
  recordCollection: Collection<
    Modify<
      Record,
      {
        _id?: ObjectId;
      }
    >
  >;
  bookingCollection: Collection<
    Modify<
      Booking,
      {
        _id?: ObjectId;
      }
    >
  >;

  constructor(
    @Inject('DATABASE_CONNECTION')
    private db: Db, // public photoUpload: AWSPhotoUploadService,
  ) {
    this.recordCollection = this.db.collection('record');
    this.bookingCollection = this.db.collection('booking');
  }

  /// TODO here should be gurad that get token off user and take id of user
  /// at the current moment we use userId instead of it

  @Mutation(() => RecordGraph)
  async createRecord(
    @Args('date', {
      type: () => GraphQLISODateTime,
    })
    date: Date,
    @Args('bookingId') bookingId: string,
    @Args('price') price: number,
    @Args('workerId') workerId: string,
    @Args('userId') userId: string,
  ): Promise<RecordGraph> {
    const _booking = await this.bookingCollection.findOne({
      _id: new ObjectId(bookingId),
    });
    const _newRecord = {
      bookingId: new ObjectId(bookingId),
      date: date,
      endDate: new Date(date.getTime() + 1000 * 60 * _booking.timePerMinute),
      price: _booking.price,
      userId: new ObjectId(userId),
      workerId: new ObjectId(workerId),
      clubId: _booking.clubId,
    };
    const _newObj = await this.recordCollection.insertOne(_newRecord);
    return new RecordGraph({ ..._newRecord, _id: _newObj.insertedId });
  }

  @Mutation(() => Boolean)
  async removeRecord(@Args('_id') _id: string): Promise<boolean> {
    await this.recordCollection.deleteOne({
      _id: new ObjectId(_id),
    });
    return true;
  }
}
