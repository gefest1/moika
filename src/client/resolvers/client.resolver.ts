import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { SMSservice } from 'src/utils/SMS/SMS.service';
import { TokenService } from '../token/token.service';
import { ClientService } from '../services/client.service';
import { Cache } from 'cache-manager';
import { CACHE_MANAGER, Inject, UseGuards } from '@nestjs/common';
import { CheckClientSMSGraph, ClientGraph } from '../models/client.model';
import { ApolloError } from 'apollo-server-express';
import { CreateClient } from '../models/createClient.args';

@Resolver()
export class ClientResolver {
  constructor(
    private smsService: SMSservice,
    private tokenService: TokenService,
    private clientService: ClientService,
    @Inject(CACHE_MANAGER) private cacheService: Cache,
  ) {}

  @Query(() => String)
  async helloWold() {
    return 'Hello World!';
  }

  @Mutation(() => String)
  async sendVerificationSMS(
    @Args('phoneNumber', { type: () => String }) phoneNumber: string,
  ): Promise<string> {
    console.log(phoneNumber);
    const filteredPhoneNumber = phoneNumber.replace(/\D/g, '');
    if (filteredPhoneNumber == '71111111111') {
      const code = '1111';
      await this.cacheService.set(`${filteredPhoneNumber}`, `${code}`, {
        ttl: 300,
      });
      return 'success';
    }
    const code =
      (await this.cacheService.get(`${filteredPhoneNumber}`)) ??
      Math.floor(1000 + Math.random() * 9000).toString();
    console.log(code);
    this.smsService.sendVerificationSMS({
      code,
      phoneNumber: filteredPhoneNumber,
    });
    await this.cacheService.set(`${filteredPhoneNumber}`, `${code}`, {
      ttl: 300,
    });
    return 'success';
  }

  @Mutation(() => CheckClientSMSGraph)
  async checkSMSVerificationCodeAsClient(
    @Args('phoneNumber', { type: () => String }) phoneNumber: string,
    @Args('code', { type: () => String }) code: string,
  ) {
    console.log('1');
    const filteredPhoneNumber = phoneNumber.replace(/\D/g, '');
    console.log('2');
    const originalCode = await this.cacheService.get(`${filteredPhoneNumber}`);
    console.log('3');
    if (code !== originalCode) throw new ApolloError('The code is wrong');
    console.log('4');
    const client = await this.clientService.findOneWithOptions({
      fields: ['phoneNumber'],
      values: [filteredPhoneNumber, { $exists: false }],
    });
    const token = this.tokenService.create({
      _id: client?._id?.toHexString(),
      phoneNumber: filteredPhoneNumber,
      role: 'client',
    });
    console.log('This is clients token' + token);
    const checkSMSResponce = new CheckClientSMSGraph({
      client: client || undefined,
      token,
    });
    return checkSMSResponce;
  }

  @Mutation(() => ClientGraph)
  async registerClient(
    @Args() args: CreateClient,
    @Args('phoneNumber', { type: () => String }) phoneNumber: string,
  ) {
    const createClient = await this.clientService.create({
      ...args,
      phoneNumber: phoneNumber,
    });

    const clientResponce = new ClientGraph({ ...createClient });
    return clientResponce;
  }
}
