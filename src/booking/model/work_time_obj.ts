import {
  Field,
  GraphQLISODateTime,
  InputType,
  Int,
  ObjectType,
} from '@nestjs/graphql';

export interface WorkTimesObj {
  date: Date;
}

@ObjectType()
export class WorkTimesObjGraph implements WorkTimesObj {
  @Field(() => GraphQLISODateTime)
  date: Date;
}

@InputType()
export class WorkTimeObjInput implements WorkTimesObj {
  @Field(() => GraphQLISODateTime)
  date: Date;
}
