import {
  ArgsType,
  Field,
  Float,
  GraphQLISODateTime,
  InputType,
  Int,
} from '@nestjs/graphql';

import { Modify } from 'src/utils/modifyType';
import { Booking } from './booking.model';
import { WorkTimeObjInput } from './work_time_obj';

@InputType()
export class BookingAgeGroupInput {
  @Field(() => Int)
  startingAge: number;

  @Field(() => Int)
  endingAge: number;
}

@InputType()
export class WorkTimesCount {}

@ArgsType()
export class CreateBooking
  implements
    Modify<
      Omit<Booking, '_id' | 'workTimes' | 'workTimesCount'>,
      {
        clubId: string;
        workTimesUserIds: string[];
      }
    >
{
  @Field()
  name: string;

  @Field({
    nullable: true,
  })
  description?: string;

  @Field()
  clubId: string;

  @Field(() => Int)
  timePerMinute: number;

  @Field(() => Float)
  price: number;

  @Field(() => GraphQLISODateTime)
  expireDate: Date;

  @Field(() => Int)
  count: number;

  @Field()
  percent: number;

  @Field(() => [String], {
    defaultValue: [],
  })
  workTimesUserIds: string[];

  @Field(() => [[WorkTimeObjInput]], { defaultValue: [] })
  workTimesObj: WorkTimeObjInput[][];
  // @Field(() => [[GraphQLISODateTime]], {
  //   defaultValue: [],
  // })
  // workTimesDates: Date[][];
}
